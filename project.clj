(defproject srimux "0.1.0-SNAPSHOT"
    :description "Automating dev workflows using tmux, vim, vim-fireplace, lein and clojure"
    :license {:name "Eclipse Public License"
              :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
                 [org.clojure/clojure "1.8.0"]
                 ])

