(ns srimux
  (:use srimux-utilities)
  (:use srimux-secrets)
  (:require [clojure.java.shell :as sh])
)

(declare wto)

(defn demo []
    ;make the srimux 2nd window into a paned window
    (resize)
    ;now all expressions ccp d in the left pane wil be sent to the right pane

    ;ts sends the command and hits enter
    (ts "ls -l")

    ;tsne just send the command
    (tsne "Enter")

    (comment
    Ctrl keys may be prefixed with ^
    Up, Down, Left, Right, BSpace, BTab, End, Enter, Escape, F1 to F12, Home, IC (Insert), 
    NPage/PageDown/PgDn, PPage/PageUp/PgUp, Space, Tab
    )
    ;send a constant
    (ts "echo \"blah\"")
    ;send a variable
    (ts (format "echo \"%s\"" supersecret-pwd))

    ;send Ctrl C and Enter
    (ts "^c")

    ;send Enter 5 times
    (dotimes [_ 5]
        (tsne "Enter")
    )

    ;write a program using vim
    ;execute it using python3
    ;use let and lexical scope
    (let [fname "/tmp/deleteme1.py"]
      (ts (format "rm %s" fname))
      (Thread/sleep 500)
      (ts (format "vim %s" fname))
      ;give vim some time to open
      (Thread/sleep 500)
      (tsne "i")
      (ts "import sys")
      (tsne "print (sys.path)")
      (tsne "Escape")
      (ts ":wq!")
      (ts (format "python3 %s" fname))
    )

    ;switch context between cpl of projects using nesting
    
    (tnewsession "project1" "test")
    (reset! session-name "srimux")
    (reset! window-number 2)
    (reset! pane-number 2)
    ;gets the tmux workspace of project 1 in the work pane
    (summon-child "project1")

    ;create another window in project 1
    (g "c")
    (g ",")
    (dotimes [_ 4]
      (tsne "BSpace")
    )
    (ts "p1 code")

    ;now create another tmux for another project
    ; but this time batch the keystrokes using do
    ; this way you wont get arthritis cpp ing 300 expressions

    (do
    (tnewsession "project2" "test")
    (reset! session-name "srimux")
    (reset! window-number 2)
    (reset! pane-number 2)
    ;gets the tmux workspace of project 1 in the work pane
    (summon-child "project2")

    ;create another window in project 2
    (Thread/sleep 500)
    (g "c")
    (g ",")
    (dotimes [_ 4]
      (tsne "BSpace")
    )
    (ts "p2 code")
    )

    ;switch between the 2
    (summon-child "project1")
    (summon-child "project2")

    ;wait till commands are finished
    ;observe how back slashes are escaped
    (ts "export PS1=\"\\u@[\\h] [\\t] [\\w] [\\$?]> \"")
    (dotimes [_ 65]
        (tsne "Enter")
    )
    (do
    (ts "sleep 10")
    ;this waits till last command is over
    (wto :nest-level 1 :attempts 90 :delay-between-polls 1000)
    (ts "ls -l")
    )
    ;in case last char is ; in a string to be sent, escape only that
    (ts "# create ;database srimux\\;")
    ;sometimes projects like project2, may have windows with tmux to say remote server
    (ts "tmux new -s p2tmux")
    ;use gg to reach the inner most tmux
    (gg ",")
    (dotimes [_ 4]
      (tsne "BSpace")
    )
    (ts "remote tmux")
    (ts "^d")
    ;you can killsession by name
    (tkillsession "project2")

    ;command pane ready
    (gohome)
)

(defn wto [ & {:keys [nest-level attempts delay-between-polls] :or {nest-level 0 attempts 20 delay-between-polls 1000}} ]
    ;waits till prev command is over as determined by PS1 changes
    ;pre requisite
    ;(ts "export PS1=\"\\u@[\\h] [\\t] [\\w] [\\$?]> \"")
    (loop [count attempts]
      (if (> count 0)
        (do
          ;(println "xxxxx")
          ;(println prompt-feedback-string)
          (let [
                prompt-feedback-string   (nth (reverse (split-lines-preserve (:out (sh/sh "tmux" "capture-pane" "-pS" "-100" "-t" (format "%s:%s" "srimux" "2.2"))))) (inc nest-level))
                [_ host tm res] (re-matches #".*\[(.*)\]\s+\[(.*)\]\s+\[(.*)\].*" prompt-feedback-string)
               ]
               (if res
                 (if (= res "0")
                   "success"
                   "failure"
                 )
                 ;no res, try again after 1 s
                 (do
                   (Thread/sleep delay-between-polls)
                   (recur (dec count))
                 )
               )
          )
        )
        (do
          "timeout"
        )
      )
    )
)
