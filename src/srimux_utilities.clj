(ns srimux-utilities
  (:require [clojure.java.shell :as sh])
)

(def window-number (atom 2))
(def session-name (atom "srimux"))
(def pane-number (atom 2))

(defn tnewsession [newsessionname firstwindowname]
  "create a new session and a first window"
  (sh/sh "tmux" "new-session" "-d" "-s" newsessionname "-n" firstwindowname)
  (reset! session-name newsessionname)
  (reset! window-number 1)
  (reset! pane-number 1)
  )

(defn tswitchsession [sessionname windownumber]
  (sh/sh "tmux" "select-window" "-t" (format "%s:%d" sessionname windownumber)) 
  (reset! session-name sessionname)
  (reset! window-number windownumber)
  (reset! pane-number 1)
)

(defn tkillsession [sessionname]
  (sh/sh "tmux" "kill-session" "-t" sessionname)
)

(defn tsend [command session & more]
  "send commands to a tmux address sessionname:windownumber.panenumber"
  (let [target (if more
	  (let [ [window-number pane-number] more]
	    (if pane-number
	      (format "%s:%d.%d" session window-number pane-number)
	      (format "%s:%d" session window-number ))
	   )
	    session 
	  )
	]
	(sh/sh "tmux" "send-keys" "-t" target command "Enter") 
    
  )
)

(defn tsend-no-enter [command session & more]
  "send commands to a tmux address sessionname:windownumber.panenumber"
  (let [target (if more
	  (let [ [window-number pane-number] more]
	    (if pane-number
	      (format "%s:%d.%d" session window-number pane-number)
	      (format "%s:%d" session window-number ))
	   )
	    session 
	  )
	]
	(sh/sh "tmux" "send-keys" "-t" target command) 
    
  )
)

(defn ts [command & more]
  (if @pane-number
    (tsend command @session-name @window-number @pane-number)
    (tsend command @session-name @window-number)
  )
)

(defn tsne [command]
  (if @pane-number
    (tsend-no-enter command @session-name @window-number @pane-number)
    (tsend-no-enter command @session-name @window-number)
  )
)

(defn g [c]
    (tsne "^a")
    (tsne c)
)

(defn gg [c]
    (tsne "^a")
    (tsne "^a")
    (tsne c)
)

(defn b [c]
    (tsne "^b")
    (tsne c)
)


(defn gohome []
      ;detach in good measure from whatever was on
      ;you can call this all day and is idempotent
      (doseq [_ (range 5) ]
        (tsne "^a")
        (tsne "d")
      )
      (doseq [_ (range 5) ]
        (tsne "Enter")
      )
)


(defn summon-child [sessionname]
    ;swap in cubelive
      ;detach from whatever is on
      (gohome)
      ;detach from whatever is on
      ;attach cubelive
      (ts (format "tmux a -t %s" sessionname))
)

(defn resize[]
    (sh/sh "tmux" "split-window" "-h" "-p" "75" "-t" (format "%s:%s" "srimux" "2")) 
    ;(sh/sh "tmux" "split-window" "-h" "-p" "10" "-t" (format "%s:%s" "cc" "2.2")) 
    ;(sh/sh "tmux" "split-window" "-v" "-p" "50" "-t" (format "%s:%s" "cc" "2.4")) 
)

(defn split-lines-preserve [s] 
  (clojure.string/split s #"\r?\n" -1)
)
